from rest_framework import serializers

from simpleapp.models import Entity


class EntitySerializer(serializers.ModelSerializer):
    password = serializers.SerializerMethodField('passwd')

    def passwd(self, entity):
        return entity.user.password

    user = serializers.StringRelatedField(many=False)

    class Meta:
        model = Entity
        fields = ('user', 'varsta', 'password')
