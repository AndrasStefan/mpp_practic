from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

from simpleapp.models import Entity
from simpleapp.serializer import EntitySerializer

redis_publisher = RedisPublisher(facility='here', broadcast=True)
message = RedisMessage('Refresh ^^')


@login_required
def index(request):
    return render(request, 'index.html', {'utilizator': request.user})


def notify(request):
    redis_publisher.publish_message(message)


class EntityList(APIView):
    def get(self, request, varsta):
        entis = Entity.objects.filter(varsta=varsta)
        serializer = EntitySerializer(entis, many=True)
        return Response(serializer.data)
