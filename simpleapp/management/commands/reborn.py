from django.core.management.base import BaseCommand

from simpleapp.models import MyUser, Entity


class Command(BaseCommand):
    help = "My shiny new management command."

    def handle(self, *args, **options):
        try:
            MyUser.objects.create_superuser("admin", "ad@yahoo.com", "admin")

            user1, user2, user3 = MyUser(username='membru1'), MyUser(username='membru2'), MyUser(username='membru3')
            user1.set_password('membru1')
            user2.set_password('membru2')
            user3.set_password('membru3')
            user1.save()
            user2.save()
            user3.save()

            Entity.objects.create(user=user1, varsta=10)
            Entity.objects.create(user=user2, varsta=20)
            Entity.objects.create(user=user3, varsta=30)

            self.stdout.write(self.style.SUCCESS('Ready to go ! :)'))
        except Exception as e:
            self.stdout.write(self.style.ERROR(e))
