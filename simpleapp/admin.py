from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from simpleapp.models import MyUser, Entity


class EntityUser(MyUser):
    class Meta:
        proxy = True
        verbose_name = 'Entitate'
        verbose_name_plural = 'Entitati'


class EntitateInLine(admin.StackedInline):
    model = Entity
    can_delete = True
    verbose_name_plural = 'Entitati'
    fk_name = 'user'


class EntitateAdmin(UserAdmin):
    inlines = (EntitateInLine, )


admin.site.register(EntityUser, EntitateAdmin)
