from django.contrib.auth.models import AbstractUser
from django.db import models


class MyUser(AbstractUser):
    def __str__(self):
        return f'{self.username} {self.email}'


class Entity(models.Model):
    user = models.OneToOneField(MyUser, models.CASCADE, related_name='entity')
    varsta = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username + " " + str(self.varsta)
