from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from simpleapp import views

app_name = 'simpleapp'
urlpatterns = [
    path('', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('index/', views.index, name='index'),
    path('notify/', views.notify, name='notify'),

    path('entis/<int:varsta>', views.EntityList.as_view()),
]
